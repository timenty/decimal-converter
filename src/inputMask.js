const inputEvents = ['insertText', 'insertFromPaste'];
const deleteEvents = ['deleteContentBackward', 'deleteByCut', 'historyUndo', 'historyRedo'];
const eventsMatch = (type, eventsList) => eventsList.includes(type);
const valueMatch = (value, pattern) => value.match(pattern) || [];

function inputMask(options = {
    "pattern": /./gm,
    "value": ""
}) {

    const pattern = new RegExp(options.pattern);
    const inputData = {
        "shadowValue": options.value
    };

    let onChangeCallback = () => '';
    let requestedValueCallback = () => inputData.value;

    Object.defineProperty(inputData, 'value', {
        set: function (val) {
            this.shadowValue = val;
            onChangeCallback(inputData);
            return val;
        },
        get: function () {
            return this.shadowValue
        },
    });
    
    const handler = e => {
        e.preventDefault();
        let {
            inputType,
            srcElement,
            data
        } = e;

        if (eventsMatch(inputType, inputEvents)) {
            inputData.value = valueMatch(srcElement.value, pattern).join('');
            if (!pattern.test(data)) {
                srcElement.value = requestedValueCallback();
            }
        }
        if (eventsMatch(inputType, deleteEvents)) {
            inputData.value = valueMatch(srcElement.value, pattern).join('');
        }
    }

    handler.onChange = function (callback) {
        onChangeCallback = typeof callback === 'function' ? callback : (() => {});
        return handler;
    }

    handler.onGetDefault = function (callback) {
        requestedValueCallback = typeof callback === 'function' ? callback : (() => inputData.value);
        return handler;
    }

    return handler;
}

export default inputMask;