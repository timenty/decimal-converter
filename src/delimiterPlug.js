function delimiter(options = {
	"regex": /.{1,4}/gm,
	"spacer": " "
}){
	const delimiterRegex = new RegExp(options.regex);
	return (val) => {
		return val ? val.match(delimiterRegex).join(options.spacer) : "";	
	}
}

export default delimiter;